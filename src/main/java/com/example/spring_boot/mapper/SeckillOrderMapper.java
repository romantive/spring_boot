package com.example.spring_boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.spring_boot.pojo.SeckillOrder;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhoubin
 * @since 2022-01-21
 */
public interface SeckillOrderMapper extends BaseMapper<SeckillOrder> {

}
