package com.example.spring_boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.spring_boot.pojo.Goods;
import com.example.spring_boot.vo.GoodsVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhoubin
 * @since 2022-01-21
 */
public interface GoodsMapper extends BaseMapper<Goods> {

    List<GoodsVo> findGoodsVo();
}
