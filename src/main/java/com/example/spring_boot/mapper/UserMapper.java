package com.example.spring_boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.spring_boot.pojo.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhoubin
 * @since 2022-01-16
 */
public interface UserMapper extends BaseMapper<User> {

}
