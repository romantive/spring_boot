package com.example.spring_boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.spring_boot.pojo.SeckillGoods;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhoubin
 * @since 2022-01-21
 */
public interface ISeckillGoodsService extends IService<SeckillGoods> {

}
